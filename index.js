console.log("JavaScript hakkas tööle")

var veaLoendur = 1;
document.querySelector('form').onsubmit = function(event) {
	event.preventDefault() // Et leht ei refreshiks

	var vastusInput = document.querySelector('input[name="vastus"]')
	var vastus = vastusInput.value
	vastusInput.value = ""
	if (vastus === "11") {
		removeHiddenFromClass('.hidden-win')
	}
	else
		document.querySelector('.viga').innerHTML = `Proovi uuesti (${veaLoendur++})`
}

document.querySelector('#next-btn').onclick = function(e) {
	console.log("Click next")
	e.target.classList.add('flicker-out-1')
	setTimeout(() => {
		revealClass('.hidden-next-1')
		setTimeout(() => {
			revealClass('.hidden-next-2')
			setTimeout(() => {
				revealClass('.hidden-next-3')
			}, 3000)
		}, 3000)
	}, 3000)
}

var removeHiddenFromClass = function(cls) {
	document.querySelectorAll(cls).forEach(item => item.classList.remove('hidden'))
}

var revealClass = function(cls) {
	removeHiddenFromClass(cls)
	document.querySelectorAll(cls).forEach(item => item.classList.add('flicker-in-1'))
}